# GitLab 8.3

---

#[fit] about.gitlab.com/release-list

---

# GitLab Pages
## EE only

---

# Auto-merge on Build Success
## CE & EE

![inline](https://about.gitlab.com/images/8_3/merge_on_pass.jpg)

---

# Contribution Analytics
## EE only

![inline](https://about.gitlab.com/images/8_3/stats.jpg)

---

# MR References in Issues
## CE & EE

![inline](https://about.gitlab.com/images/8_3/references.jpg)

---

# Issue weight
## EE only

![inline](https://about.gitlab.com/images/8_3/weights1.jpg)

---

# MR from web editor
## CE & EE

![inline](https://about.gitlab.com/images/8_3/new_mr.jpg)
