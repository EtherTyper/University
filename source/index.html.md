---
title: GitLab University
---
 
## What is GitLab University

Version control will be the standard for creation of any type of media in the
future. Today it's still pretty hard to master.

GitLab University has as a goal to teach Git, GitLab and everything that relates
to that to anyone willing to listen and participate.

For more information on the process of creating these materials, please refer to
the [Process Documentation](/process).

---

### Table of Contents

- [General Boot Camp](#general-boot-camp)
    - [Stage 1](#stage-1)
    - [Stage 2](#stage-2)
    - [Stage 3](#stage-3)
    - [Stage 4](#stage-4)
    - [Other general topics](#other-general-topics)
        - [GitLab Wiki](#gitlab-wiki)
        - [GitLab Activity Log](#gitlab-activity-log)
        - [Branching and Forking](#branching-and-forking)
        - [GitLab Omnibus](#gitlab-omnibus)
        - [Continuous Integration and Runners](#continuous-integration-and-runners)
        - [GitLab Mirroring](#gitlab-mirroring)
        - [GitLab Pages](#gitlab-pages)
        - [GitLab Markdown](#gitlab-markdown)
        - [Extra Topics](#extra-topics)
- [Training Material](#training-material)
    - [User Training](#user-training)
- [Support Boot Camp](/support)
- [Sales Boot Camp](/sales)

---

## General Boot Camp <a name="general-boot-camp"></a>

In order to get everyone started quickly with GitLab, an intense 4 stages course
has been prepared. The path outlined here can be followed by everyone new to GitLab,
and depending on your functional group, extra steps will be required.

These extra steps are described in their own document and linked from here, when
necessary.

#### Recordings

There is a [YouTube playlist with all recordings](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg/playlists),
and links to specific videos can be found bellow.

#### Webcasts

There is a university class every Thursday at 5PM UTC for GitLab team members
only. Send Job a message to sign up.

---

### Stage 1 <a name="stage-1"></a>

**Familiarize yourself with the basics**

1. Understanding [terminology](/glossary/).
   Please create a merge request for any term that does not have an answer or
   to add a term that you feel is relevant but is not on this page.

1. Understanding Version Control Systems
    - [Presentation](https://docs.google.com/presentation/d/16sX7hUrCZyOFbpvnrAFrg6tVO5_yT98IgdAqOmXwBho/edit#slide=id.g72f2e4906_2_29)

1. Operating Systems and How Git Works
    - [Operating Systems and How Git Works](https://drive.google.com/a/gitlab.com/file/d/0B41DBToSSIG_OVYxVFJDOGI3Vzg/view?usp=sharing)
      recorded date: 2015-10-01

1. Introduction to Git
    - [Intro to Git](https://www.codeschool.com/account/courses/try-git)
    - Supporting: [GitLab Basics](http://doc.gitlab.com/ce/gitlab-basics/README.html)

1. [GitLab Workshop Part 1: Basics of Git and GitLab](https://courses.platzi.com/classes/git-gitlab/concepto/part-1/part-1/material/)

1. [GitLab Workshop Part 2: Basics of Git and GitLab](https://courses.platzi.com/classes/git-gitlab/concepto/part-1/part-23370/material/)

1. [GitLab Workshop Part 3: Basics of Git and GitLab](https://courses.platzi.com/classes/git-gitlab/concepto/part-1/part-3/material/)

1. [GitLab Workshop Part 4: Basics of Git and GitLab](https://courses.platzi.com/classes/git-gitlab/concepto/part-1/part-4/material/)

1. [Making GitLab Great for Everyone, our response to the Dear GitHub letter](https://www.youtube.com/watch?v=GGC40y4vMx0)

1. Create a GitLab Account
    - [Create a GitLab Account](https://courses.platzi.com/classes/git-gitlab/concepto/first-steps/create-an-account-on-gitlab/material/)
    - Supporting: [Demo of GitLab.com](https://www.youtube.com/watch?v=WaiL5DGEMR4)

1. Add SSH key to GitLab
    - [Create and Add your SSH key to GitLab](https://www.youtube.com/watch?v=54mxyLo3Mqk)

1. Repositories, Projects and Groups
    - [Recording](https://www.youtube.com/watch?v=4TWfh1aKHHw&index=1&list=PLFGfElNsQthbQu_IWlNOxul0TbS_2JH-e)
      recorded date: 2015-12-10

1. Creating a Project in GitLab
    - [Recording](https://www.youtube.com/watch?v=7p0hrpNaJ14)
    - Supporting: [Demo of GitLab.com](https://www.youtube.com/watch?v=WaiL5DGEMR4)

1. Issues and Merge Requests
    - [Recording](https://www.youtube.com/watch?v=raXvuwet78M)
    - Supporting: [Demo of GitLab.com](https://www.youtube.com/watch?v=WaiL5DGEMR4)

### Stage 2 <a name="stage-2"></a>

**Experience first hand the power of GitLab**

1. Ecosystem
    - [Recording 2015-11-05](https://www.youtube.com/watch?v=sXlhgPK1NTY&list=PLFGfElNsQthbQu_IWlNOxul0TbS_2JH-e&index=6)
    - [GitLab Ecosystem slides](https://docs.google.com/presentation/d/1vCU-NbZWz8NTNK8Vu3y4zGMAHb5DpC8PE5mHtw1PWfI/edit)

1. [Compare GitLab versions](/features/#compare)

1. [GitLab compared to other tools](/comparison/)
    - [Compared to Atlassian (Recording 2016-03-03) ](https://youtu.be/Nbzp1t45ERo)

1. [Positioning FAQ](https://about.gitlab.com/handbook/positioning-faq)

1. JIRA and Jenkins integrations in GitLab
    - [Demo of Jira integration within GitLab](https://gitlabmeetings.webex.com/gitlabmeetings/ldr.php?RCID=44b548147a67ab4d8a62274047146415).
      Download [WebEx](https://www.webex.com/play-webex-recording.html) to view this video

1. GitLab Flow
    - [Recording of what it is](https://www.youtube.com/watch?v=UGotqAUACZA)
    - [GitLab Flow blog post](/2014/09/29/gitlab-flow/)
    - [GitLab Flow documentation](http://doc.gitlab.com/ee/workflow/gitlab_flow.html)

1. GitLab Integrations
    - Supporting: [Documentation on Integrating Jira with GitLab](http://doc.gitlab.com/ee/integration/jira.html) and [Demo of Jira integration within GitLab](https://gitlabmeetings.webex.com/gitlabmeetings/ldr.php?RCID=44b548147a67ab4d8a62274047146415).  Download [WebEx](https://www.webex.com/play-webex-recording.html) to view this video
    - Supporting: [Atlassian Crowd feature request](http://feedback.gitlab.com/forums/176466-general/suggestions/4324384-integration-with-crowd)
    - Supporting: [Documentation on Integrating Jenkins with GitLab](http://doc.gitlab.com/ee/integration/jenkins.html)
    - Supporting: [Documentation on Integrating Bamboo with GitLab](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/project_services/bamboo.md)
    - Supporting: [Documentation on Integrating Slack with GitLab](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/integration/slack.md)

1. Migrating to GitLab from SVN/GitHub Enterprise/BitBucket/Perforce
    - Supporting: [Migrating from BitBucket/Stash](http://doc.gitlab.com/ee/workflow/importing/import_projects_from_bitbucket.html)
    - Supporting: [Migrating from GitHub](http://doc.gitlab.com/ee/workflow/importing/import_projects_from_github.html)
    - Supporting: [Migrating from SVN](http://doc.gitlab.com/ee/workflow/importing/migrating_from_svn.html)
    - Supporting: [Migrating from Fogbugz](http://doc.gitlab.com/ee/workflow/importing/import_projects_from_fogbugz.html)

1. Follow the steps outlined on **Stage 2** of your path:
    - [Support Path](/support/)
    - [Sales Path](/sales/)

### Stage 3 <a name="stage-3"></a>

**Get familiar with GitLab's advanced features**

1. [Compare GitLab versions](https://about.gitlab.com/features/#compare)

1. Scalability and High Availability
    - [Recording 2015-12-03](https://www.youtube.com/watch?v=cXRMJJb6sp4&list=PLFGfElNsQthbQu_IWlNOxul0TbS_2JH-e&index=2)
    - [High Availability Documentation](https://about.gitlab.com/high-availability/)

1. Managing LDAP, Active Directory
    - [Recording of what it is and how to set it up](https://www.youtube.com/watch?v=HPMjM-14qa8)

1. Managing Permissions within EE
    - [Recording of what it is and how to set it up](https://www.youtube.com/watch?v=DjUoIrkiNuM)

1. GitLab 8.2
    - [Recording 2015-11-19](https://www.youtube.com/watch?v=09RLHyMFfpA&index=3&list=PLFGfElNsQthbQu_IWlNOxul0TbS_2JH-e)
    - [Slides](https://gitlab.com/gitlab-org/University/blob/master/classes/8.2.markdown) 2015-11-19

1. Upcoming in EE and Big files in Git (Git LFS, Annex)
    - [Upcoming in EE](https://gitlab.com/gitlab-org/University/blob/master/classes/upcoming_in_ee.md)
    - [Big files in Git (Git LFS, Annex)](https://gitlab.com/gitlab-org/University/blob/master/classes/git_lfs_and_annex.md)

1. Follow the steps outlined on **Stage 3** of your path:
    - [Support Path](/support/)
    - [Sales Path](/sales/)

### Stage 4 <a name="stage-4"></a>

**Dive into your particular path**

- [Support Path](/support/)
- [Sales Path](/sales/)

### Other general topics <a name="other-general-topics"></a>

#### GitLab Wiki <a name="gitlab-wiki"></a>

- Upcoming

#### GitLab Activity Log <a name= "gitlab-activity-log"></a>

- Upcoming

#### Branching and Forking <a name="branching-and-forking"></a>

- Upcoming

#### GitLab Omnibus <a name="gitlab-omnibus"></a>

- [Recording of what it is](https://www.youtube.com/watch?v=XTmpKudd-Oo)
- [Recording of how to install](https://www.youtube.com/watch?v=Q69YaOjqNhg)
- Supporting: [Configuring an external PostgreSQL database](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/README.md#using-a-non-packaged-postgresql-database-management-server)
- Supporting: [Configuring an external MySQL database](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/README.md#using-a-mysql-database-management-server-enterprise-edition-only)

#### Continuous Integration and Runners <a name="continuous-integration-and-runners"></a>

- [Continuous Delivery vs Continuous Deployment](https://www.youtube.com/watch?v=igwFj8PPSnw)
- [GitLab CI product page](/gitlab-ci/)
- [Setting up GitLab Runner For Continuous Integration](/2016/03/01/gitlab-runner-with-docker/?mkt_tok=3RkMMJWWfF9wsRonsqnLZKXonjHpfsX56%2BwpWKW%2FlMI%2F0ER3fOvrPUfGjI4AScJqI%2BSLDwEYGJlv6SgFS7nBMbZ22bgPWRA%3D)
- Supporting: [Documentation on Integrating Jenkins with GitLab](http://doc.gitlab.com/ee/integration/jenkins.html)
- Supporting: [Documentation on Integrating Bamboo with GitLab](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/project_services/bamboo.md)
- Supporting: [Article on Continuous Integration from ThoughtWorks](https://www.thoughtworks.com/continuous-integration)

#### GitLab Mirroring <a name="gitlab-mirroring"></a>

- Upcoming

#### GitLab Pages <a name="gitlab-pages"></a>

- Upcoming
- Supporting: [Documentation on How to set up and use Pages](http://doc.gitlab.com/ee/pages/README.html)

#### GitLab Markdown <a name="gitlab-markdown"></a>

- Upcoming
- Supporting: [Markdown](http://doc.gitlab.com/ce/markdown/markdown.html)

#### Extra Topics <a name="extra-topics"></a>

* [GitLab documentation](http://doc.gitlab.com/)
* [GitLab architecture for noobs](https://dev.gitlab.org/gitlab/gitlabhq/blob/master/doc/development/architecture.md)
* [Innersourcing article](https://about.gitlab.com/2014/09/05/innersourcing-using-the-open-source-workflow-to-improve-collaboration-within-an-organization/)
* [Platzi training course](https://courses.platzi.com/courses/git-gitlab/)
* [GitLab Direction](https://about.gitlab.com/direction/)
* [Amazon's transition to Continuous Delivery](https://www.youtube.com/watch?v=esEFaY0FDKc)
* [3rd party tool comparison](http://technologyconversations.com/2015/10/16/github-vs-gitlabs-vs-bitbucket-server-formerly-stash/)
* [State of Dev Ops 2015 Report by Puppet Labs](https://puppetlabs.com/sites/default/files/2015-state-of-devops-report.pdf) Insightful Chapters to understand the Impact of Continuous Delivery on Performance (Chapter 4), the Application Architecture (Chapter 5) and How IT Managers can help their teams win (Chapter 6).
* [Customer review of GitLab with talking points on why they prefer GitLab](https://www.enovate.co.uk/web-design-blog/2015/11/25/gitlab-review/)
* [2011 WSJ article by Mark Andreeson - Software is Eating the World](http://www.wsj.com/articles/SB10001424053111903480904576512250915629460)
* [2014 Blog post by Chris Dixon - Software eats software development](http://cdixon.org/2014/04/13/software-eats-software-development/)
* [2015 Venture Beat article - Actually, Open Source is Eating the World](http://venturebeat.com/2015/12/06/its-actually-open-source-software-thats-eating-the-world/)
* [Customer Use-Cases](https://about.gitlab.com/handbook/use-cases/)
* [Why Git and GitLab slide deck](https://docs.google.com/a/gitlab.com/presentation/d/1RcZhFmn5VPvoFu6UMxhMOy7lAsToeBZRjLRn0LIdaNc/edit?usp=drive_web)
* [Git Workshop](https://docs.google.com/presentation/d/1JzTYD8ij9slejV2-TO-NzjCvlvj6mVn9BORePXNJoMI/edit?usp=drive_web)
* [Client Assessment of GitLab versus GitHub](https://docs.google.com/a/gitlab.com/spreadsheets/d/18cRF9Y5I6I7Z_ab6qhBEW55YpEMyU4PitZYjomVHM-M/edit?usp=sharing) INTERNAL ACCESS ONLY

## Training Material <a name="training-material"></a>

### User Training <a name="user-training"></a>

Git workshop for GitLab end-users. This training covers everything users need
to configure their environment and understand the GitLab interface.

- [Slides](https://gitlab.com/gitlab-org/University/blob/master/training/user_training.md)
can be viewed using [Deckset](http://www.decksetapp.com/).
